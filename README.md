### Markov Chains

This is a simple implementation of markov chains.
An example chain could be generated with bin/gen_chain.
The main program expect a file specified in src/main.c (default is files/in.txt).
The program generates a markov chain using the file.
Output and other things can be tweaked with the macros in the files.

As a good example of my programs abilities is the aristocrats south park joke ;)
even though it is short, longer files will produce better output

### Build

To build use
``` ./build.sh ```
to build and run use
``` ./build.sh run ```

See the build.sh file for more info

### Limitations

Limitations of the output:
1. No upper case letters (who needs them anyway)
2. No proper punctuation (who needs punctuation anyway), just words

Limitations of processing input:
1. The symbols: `[]"", ` are completely ignored,
`.` and `\n` are used to identify end of thought (sentance)
