#include <stdio.h>
#include <time.h>

#define PRINT_ITEM_FREQUENCY
// #define PRINT_ON_WALK
#define SAVE_TO_FILE_ON_WALK "files/in.txt"
#define WALK_LEN 10000

#define ITEM_CAP 3
int ITEMS = 3;

double chain[ITEM_CAP][ITEM_CAP] = {
    { 0.2, 0.6, 0.2 },
    { 0.3, 0.0, 0.7 },
    { 0.5, 0.0, 0.5 }
};

char item_names[ITEM_CAP][64] = {
    "Burger",
    "Pizza",
    "Hotdog"
};

#include "markov.h"

int main(void)
{
    srand(time(NULL));

    print_chain();
    take_walk();
    return 0;
}
