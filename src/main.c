#include <stdio.h>
#include <time.h>

#define PRINT_ITEM_FREQUENCY
// #define PRINT_ON_WALK
// #define SAVE_TO_FILE_ON_WALK "out.txt"
#define WALK_LEN 10000

#define ITEM_CAP 400
int ITEMS = 0;

double chain[ITEM_CAP][ITEM_CAP] = {0};
char item_names[ITEM_CAP][64] = {0};

#include "markov.h"

int main(void)
{
    srand(time(NULL));
    generate_chain("files/in.txt");

    // print_chain();
    take_walk();
    return 0;
}
