#!/bin/sh

cd ${0%/*} # go to project root

FLAGS="-Wall -Wextra -g -pedantic"
SRCD="src"
BIN="bin"
FILES="files"
RUN=0

function __run__ {
    RUN=1
}

function __clean__ {
    rm -rf $BIN
    rm -rf $FILES/*.txt
    kill $( ps -q $$ -o pgid= ) # exit
}

set -xe

if ! { [[ $# -eq 0 ]]; } 2> /dev/null
then
    __$1__
fi


mkdir -p $BIN
mkdir -p $FILES

gcc -o $BIN/main      $FLAGS $SRCD/main.c
gcc -o $BIN/gen_chain $FLAGS $SRCD/gen_chain.c

if ! { [[ $RUN -eq 0 ]]; } 2> /dev/null
then
    $BIN/gen_chain
    $BIN/main
fi
